# -*- coding: utf-8 -*-
import os
import timeit
import json
from tqdm import tqdm
from datetime import datetime
from treelib import Tree
from collections import defaultdict

from opencc import OpenCC

project_path = os.path.abspath(os.path.dirname(__file__))


def get_data():
    base_dir = os.path.normpath(os.path.join(project_path, "datasets/output/"))
    category_file = os.path.join(base_dir, "Category.txt")
    category_outlink_file = os.path.join(base_dir, "category_outlinks.txt")
    category_inlink_file = os.path.join(base_dir, "category_inlinks.txt")
    category2pages_file = os.path.join(base_dir, "category_pages.txt")
    page_map_line_file = os.path.join(base_dir, "PageMapLine.txt")
    #
    cc = OpenCC('t2s')
    #
    category_id2name = dict()
    with open(category_file, encoding="utf8")as fi:
        for line in fi.readlines():
            parts = line.rstrip().split("\t")
            category_id2name[parts[1]] = parts[2]
    #
    out_links_dict = defaultdict(list)
    with open(category_outlink_file, encoding="utf8")as fi:
        for line in fi.readlines():
            parts = line.rstrip().split("\t")
            out_links_dict[parts[0]].append(parts[1])
    #
    in_links_dict = defaultdict(list)
    with open(category_inlink_file, encoding="utf8")as fi:
        for line in fi.readlines():
            parts = line.rstrip().split("\t")
            in_links_dict[parts[0]].append(parts[1])
    #
    category2pages = defaultdict(list)
    with open(category2pages_file, encoding="utf8")as fi:
        for line in fi.readlines():
            parts = line.rstrip().split("\t")
            category2pages[parts[0]].append(parts[1])
    #
    page_id2title = dict()
    with open(page_map_line_file, encoding="utf8")as fi:
        for line in fi.readlines():
            parts = line.rstrip().split("\t")
            page_id2title[parts[0]] = parts[1]
    #
    for k in out_links_dict:
        out_links_dict[k] = list(set(out_links_dict[k]))
    #
    print("总类别数：{}，子类别链接数：{}".format(len(category_id2name), len(out_links_dict)))
    return category_id2name, out_links_dict, in_links_dict, category2pages, page_id2title


def do_make_schema_by_tree(root_node, is_print=False):
    """
    建立树状的schema，但是不太靠谱，弃用
    :param root_node:
    :param is_print:
    :return:
    """
    tree = Tree()
    tree.create_node(root_node[0], root_node[1])
    category_id2name, out_links_dict, _, category2pages, page_id2title = get_data()
    stack = [root_node]
    count = 0
    start_time = timeit.default_timer()
    while stack:
        curr_node = stack.pop(-1)
        curr_id = curr_node[1]
        next_ids = out_links_dict.get(curr_id, [])
        if len(next_ids) == 0:
            # 叶子节点
            next_ids = category2pages.get(curr_id, [])
            if len(next_ids) == 0:
                pass
            else:
                next_tags = [page_id2title.get(i, "") for i in next_ids]
                for tag, _id in zip(next_tags, next_ids):
                    try:
                        tree.create_node(tag, _id, curr_id)
                    except Exception as e:
                        pass
        else:
            next_tags = [category_id2name.get(i, "") for i in next_ids]
            #
            for tag, _id in zip(next_tags, next_ids):
                try:
                    tree.create_node(tag, _id, curr_id)
                    stack.append((tag, _id))
                except Exception as e:
                    pass
                    # print(e.__str__())
        count += 1
        if is_print and count % 50 == 0:
            curr_time = timeit.default_timer()
            print("{0}, 循环次数：{1}, 当前栈中的节点数：{2}, 当前总耗时：{3:.3f} 秒.".format(
                datetime.now(), count, len(stack), curr_time - start_time))
    #
    # tree.show()
    print("{}构建Schema完成. Schema层数：{}".format(datetime.now(), tree.depth()))
    #
    json_str_result = tree.to_json(with_data=False)
    json_result = json.loads(json_str_result)
    with open("tree.json", "w", encoding="utf8")as fo:
        json.dump(json_result, fo, ensure_ascii=False, indent=2)
    tree.save2file("tree.txt")


def do_make_schema_by_graph(start_node, is_print=False):
    """
    建立图状的schema
    :param start_node:
    :param is_print:
    :return:
    """
    #
    def make_node(head, head_id, relation, tail, tail_id):
        return {"head": head, "head_id": head_id, "head_type": "disease",
                "tail": tail, "tail_id": tail_id, "tail_type": "disease",
                "relation": relation}
    #
    exclude_tags = {"伊斯兰教","依疾病划分的医疗与健康组织",
                    "各身心状态人物","病死者", "疾病小作品","疾病題材作品", "各国疾病和病症", "疾病疫情"}
    exclude_keywords = ["的人", "逝世者", "游戏", "電影", "电影", "建筑", "动画",
                        "小说", "漫画", "电视剧", "铁路车站", "铁路线", "主教", "教宗", "枢机"]
    category_id2name, out_links_dict, in_links_dict, category2pages, page_id2title = get_data()

    def _find_all_related_nodes(start_node, is_print=False):
        """
        找到所有与疾病有关的id
        :param start_node:
        :param is_print:
        :return:
        """
        nodes = set()
        nodes.add(start_node[1])
        leaves = set()
        stack = [start_node]
        count = 0
        while stack:
            curr_node = stack.pop(-1)
            curr_id = curr_node[1]
            if "4381675" == curr_id:
                a = 1
            curr_tag = curr_node[0]
            children_ids = out_links_dict.get(curr_id, [])
            if len(children_ids) == 0:
                # 叶子结点
                children_ids = category2pages.get(curr_id, [])
                if len(children_ids) == 0:
                    pass
                else:
                    children_tags = [page_id2title.get(i, "") for i in children_ids]
                    for tag, _id in zip(children_tags, children_ids):
                        if "" == tag:
                            continue
                        is_found = False
                        for k in exclude_keywords:
                            if k in tag:
                                is_found = True
                                break
                        if tag not in exclude_tags and (not is_found) and (tag, _id) not in stack and _id not in leaves:
                            stack.append((tag, _id))
                            leaves.add(_id)
                        else:
                            pass
            else:
                children_tags = [category_id2name.get(i, "") for i in children_ids]
                for tag, _id in zip(children_tags, children_ids):
                    if "" == tag:
                        continue
                    is_found = False
                    for k in exclude_keywords:
                        if k in tag:
                            is_found = True
                            break
                    if tag not in exclude_tags and (not is_found) and (tag, _id) not in stack and _id not in nodes:
                        stack.append((tag, _id))
                        nodes.add(_id)
                    else:
                        pass
            #
            count += 1
            if is_print and count % 100 == 0:
                print("{}, 当前循环数：{}, 当前类别节点数：{}, 当前叶子节点数：{}".format(datetime.now(), count, len(nodes), len(leaves)))
        #
        print("{}， 找到与疾病相关的类别节点数：{}, 叶子节点数：{}".format(datetime.now(), len(nodes), len(leaves)))
        if is_print:
            print("类别节点示例：\n{}".format("\n".join([category_id2name.get(i, "") for i in list(nodes)[:10]])))
            print("叶子节点示例：\n{}".format("\n".join([page_id2title.get(i, "") for i in list(leaves)[:10]])))
        #
        with open("nodes.txt", "w", encoding="utf8")as fo:
            fo.write("\n".join([str((i, category_id2name.get(i, ""))) for i in list(nodes)]))
        with open("leaves.txt", "w", encoding="utf8")as fo:
            fo.write("\n".join([str((i, page_id2title.get(i, ""))) for i in list(leaves)]))
        return nodes, leaves
    #
    # test
    cc = OpenCC('t2s')
    print(cc.convert("轉換"))
    nodes, leaves = _find_all_related_nodes(start_node, is_print=True)
    total_nodes = nodes | leaves
    result = set()
    for head_id, tail_ids in tqdm(out_links_dict.items()):
        for tail_id in tail_ids:
            if head_id in total_nodes and tail_id in total_nodes:
                new_node = make_node(category_id2name.get(head_id, "dummy"),
                                     head_id,
                                     "hasA",
                                     category_id2name.get(tail_id, "dummy"),
                                     tail_id)
                result.add(str(new_node))
    for tail_id, head_ids in tqdm(in_links_dict.items()):
        for head_id in head_ids:
            if head_id in total_nodes and tail_id in total_nodes:
                new_node = make_node(category_id2name.get(head_id, "dummy"),
                                     head_id,
                                     "hasA",
                                     category_id2name.get(tail_id, "dummy"),
                                     tail_id)
                result.add(str(new_node))
    for head_id, tail_ids in tqdm(category2pages.items()):
        for tail_id in tail_ids:
            if head_id in total_nodes and tail_id in total_nodes:
                new_node = make_node(category_id2name.get(head_id, "dummy"),
                                     head_id,
                                     "hasA",
                                     page_id2title.get(tail_id, "dummy"),
                                     tail_id)
                result.add(str(new_node))
    # 循环结束

    print("\n{}, 获取三元组数：{}".format(datetime.now(), len(result)))
    result = [eval(item) for item in list(result)]
    with open("triples.txt", "w", encoding="utf8")as fo:
        step = 10
        for i in range(0, len(result), step):
            fo.write(json.dumps({"predict": result[i: i + step]}, ensure_ascii=False) + "\n")
            if i % 100 == 0:
                fo.flush()


#
if __name__ == "__main__":
    start_node = ("各系统疾病", "1745070")
    do_make_schema_by_tree(start_node, is_print=True)
    # do_make_schema_by_graph(start_node, is_print=True)
    print("{}, done.".format(datetime.now()))
